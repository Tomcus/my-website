use anyhow::Result;
use axum::Server;
use std::net::SocketAddr;
use std::path::PathBuf;
use std::sync::Arc;

use axum_utils::delivery::StaticFilesRouter;

async fn start_server(runtime: Arc<tokio::runtime::Runtime>) -> Result<()> {
    let statics_path = PathBuf::from("./static");
    let statics = StaticFilesRouter::new(runtime.clone(), &statics_path).await?;
    let statics = Arc::new(statics);
    let statics_router = statics.create_router();
    let addr = SocketAddr::from(([0, 0, 0, 0], 8080));
    Server::bind(&addr)
        .serve(statics_router.into_make_service())
        .await?;
    return Ok(());
}

fn main() -> Result<()> {
    let runtime = Arc::new(tokio::runtime::Runtime::new()?);
    runtime.block_on(start_server(runtime.clone()))?;

    return Ok(());
}
