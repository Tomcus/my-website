use anyhow::Result;
use axum::http::Response;
use std::path::PathBuf;
use tokio::fs;

#[derive(Debug)]
pub struct StaticFile {
    content: String,
    content_type: &'static str,
}

impl StaticFile {
    fn file_extention_to_content_type(extention: &str) -> &'static str {
        match extention {
            "html" => "text/html",
            "css" => "text/css",
            "txt" => "text/plain",
            "js" => "application/javascript",
            "json" => "application/json",
            _ => todo!(),
        }
    }

    pub async fn load(path: &PathBuf) -> Result<Self> {
        let content = fs::read_to_string(path).await?;
        let extention = path
            .extension()
            .map(|ext| ext.to_str().unwrap())
            .unwrap_or("txt");
        let content_type = Self::file_extention_to_content_type(extention);
        return Ok(Self {
            content,
            content_type,
        });
    }

    pub fn templated_html(content: String) -> Self {
        return Self {
            content,
            content_type: "text/html",
        };
    }

    pub fn create_response(&self) -> Result<Response<String>, axum::http::Error> {
        return Response::builder()
            .header("Content-Type", self.content_type)
            .body(self.content.clone());
    }
}
