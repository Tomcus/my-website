use crate::delivery::file::StaticFile;
use crate::delivery::scan::ScannedDirectory;

use anyhow::Result;
use axum::http::{Response, StatusCode};
use minijinja::{context, Environment};
use std::collections::HashMap;
use std::path::Path;
use tokio::fs;

#[derive(Debug)]
pub struct StaticFiles {
    templates: HashMap<String, String>,
    files: HashMap<String, StaticFile>,
}

impl StaticFiles {
    async fn try_from(scanned_dir: ScannedDirectory) -> Result<Self> {
        let mut res = Self {
            templates: HashMap::new(),
            files: HashMap::new(),
        };

        for template_file in &scanned_dir.templates {
            let content = fs::read_to_string(&template_file).await?;
            let name = Self::get_template_name_from_path(template_file).to_string();
            res.templates.insert(name, content);
        }

        let mut jinja = Environment::new();
        for template_file in scanned_dir.templates {
            let name = Self::get_template_name_from_path(&template_file);
            if let Some((name, content)) = res.templates.get_key_value(name) {
                jinja.add_template(name, content)?;
            }
        }

        for file in scanned_dir.files {
            if let Some(Some(extention)) = file.extension().map(|ext| ext.to_str()) {
                if extention == "html" {
                    let path = Self::normalize_path(&file);
                    let content = fs::read_to_string(&file).await?;
                    let content = jinja.render_str(&content, context!())?;
                    res.files
                        .insert(path.to_string(), StaticFile::templated_html(content));
                    continue;
                }
            }
            let path = Self::normalize_path(&file);
            res.files
                .insert(path.to_string(), StaticFile::load(&file).await?);
        }

        return Ok(res);
    }

    fn get_template_name_from_path(path: &Path) -> &str {
        let path = path.parent().unwrap();
        let path = path.to_str().unwrap();
        let path = path.strip_prefix("./static").unwrap_or(path);
        if path.is_empty() {
            return "index";
        } else {
            return path;
        }
    }

    fn normalize_path(path: &Path) -> &str {
        let path = path.to_str().unwrap();
        let path = path.strip_prefix("./static").unwrap_or(path);
        let path = path.strip_suffix(".html").unwrap_or(path);
        let path = path.strip_suffix("index").unwrap_or(path);
        return path;
    }

    pub async fn load(source: &Path) -> Result<Self> {
        return Self::try_from(ScannedDirectory::scan(source).await?).await;
    }

    pub fn get_response(&self, file_path: &str) -> Result<Response<String>, StatusCode> {
        if let Some(file) = self.files.get(file_path) {
            if let Ok(response) = file.create_response() {
                return Ok(response);
            } else {
                return Err(StatusCode::INTERNAL_SERVER_ERROR);
            }
        } else {
            return Err(StatusCode::NOT_FOUND);
        }
    }
}
