mod file;
pub mod files;
mod scan;

use files::StaticFiles;

use anyhow::Result;
use axum::{
    extract::{OriginalUri, State},
    http::StatusCode,
    response::Response,
    routing::get,
    Router,
};
use notify::{event::EventKind, inotify::INotifyWatcher, RecursiveMode, Watcher};
use std::path::{Path, PathBuf};
use std::sync::Arc;
use tokio::sync::watch;
use tower_http::{
    trace::{DefaultMakeSpan, DefaultOnRequest, DefaultOnResponse, TraceLayer},
    LatencyUnit,
};
use tracing::Level;

pub struct StaticFilesRouter {
    _watcher: INotifyWatcher,
    reciever: watch::Receiver<StaticFiles>,
}

async fn serve_static(
    State(state): State<Arc<StaticFilesRouter>>,
    OriginalUri(url): OriginalUri,
) -> Result<Response<String>, StatusCode> {
    println!("{url}");
    let url = url.to_string();
    let files = state.reciever.borrow();
    return files.get_response(&url);
}

impl StaticFilesRouter {
    async fn load_files(static_files_path: &Path) -> Result<StaticFiles> {
        StaticFiles::load(static_files_path).await
    }

    async fn create_channels(
        static_files_path: &Path,
    ) -> Result<(watch::Sender<StaticFiles>, watch::Receiver<StaticFiles>)> {
        Ok(watch::channel(Self::load_files(static_files_path).await?))
    }

    pub async fn new(
        runtime: Arc<tokio::runtime::Runtime>,
        static_files_path: &Path,
    ) -> Result<Self> {
        let (sender, reciever) = Self::create_channels(static_files_path).await?;
        let watcher_path = PathBuf::from(&static_files_path);
        let mut watcher = notify::recommended_watcher(
            move |res: Result<notify::Event, notify::Error>| match res {
                Ok(event) => {
                    match event.kind {
                        EventKind::Modify(_) | EventKind::Remove(_) | EventKind::Create(_) => {
                            println!("INFO: Reloading");
                            match runtime.block_on(Self::load_files(&watcher_path)) {
                                Ok(files) => {
                                    match runtime.block_on(async {
                                        return sender.send(files);
                                    }) {
                                        Ok(_) => println!("INFO: Reload successfull"),
                                        Err(e) => eprintln!("ERROR: Reload failed {e:?}"),
                                    }
                                }
                                Err(e) => eprintln!("ERROR: Reload failed: {e:?}"),
                            }
                        }
                        _ => {}
                    };
                }
                Err(e) => eprintln!("ERROR: watch error: {e:?}"),
            },
        )?;
        watcher.watch(static_files_path, RecursiveMode::Recursive)?;
        Ok(Self {
            _watcher: watcher,
            reciever,
        })
    }

    pub fn create_router(self: Arc<Self>) -> Router {
        let tracer = TraceLayer::new_for_http()
            .make_span_with(DefaultMakeSpan::new().include_headers(true))
            .on_request(DefaultOnRequest::new().level(Level::INFO))
            .on_response(
                DefaultOnResponse::new()
                    .level(Level::INFO)
                    .latency_unit(LatencyUnit::Micros),
            );
        return Router::new()
            .layer(tracer)
            .route("/", get(serve_static))
            .route("/*any", get(serve_static))
            .with_state(self); // Shared state must be specified at the end.
    }
}
