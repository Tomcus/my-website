use anyhow::Result;
use std::{
    collections::VecDeque,
    path::{Path, PathBuf},
};
use tokio::fs;
use tokio_stream::{wrappers::ReadDirStream, StreamExt};

pub struct ScannedDirectory {
    pub templates: Vec<PathBuf>,
    pub files: Vec<PathBuf>,
}

impl ScannedDirectory {
    pub async fn scan(path_to_dir: &Path) -> Result<Self> {
        let path_to_dir: PathBuf = path_to_dir.into();
        let mut templates = vec![];
        let mut files = vec![];
        let mut queue = VecDeque::new();
        queue.push_back(path_to_dir);
        while let Some(scanning_directory) = queue.pop_front() {
            let mut stream = ReadDirStream::new(fs::read_dir(scanning_directory).await?);
            while let Some(file_or_dir) = stream.next().await {
                let file_or_dir = file_or_dir?;
                let metadata = file_or_dir.metadata().await?;
                if metadata.is_file() {
                    let file_path = file_or_dir.path();
                    if file_path.ends_with(".template.html") {
                        templates.push(file_path);
                    } else {
                        files.push(file_path);
                    }
                } else if metadata.is_dir() {
                    queue.push_back(file_or_dir.path());
                } else if metadata.is_symlink() {
                    // TODO: follow symlinks
                }
            }
        }
        return Ok(Self { templates, files });
    }
}
